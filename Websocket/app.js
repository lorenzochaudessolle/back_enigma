var http = require('http');
var fs = require('fs');
var app = require('express')();
var server = require('http').Server(app);


// Chargement de socket.io
var io = require('socket.io').listen(server);
var count = 0;
var $clientConnected = [];

io.on('connection', function (socket) {
  // var $ipAddress = socket.client;
  count++;
  io.emit('counter', { count: count });
  $clientConnected.push(socket.client.id)
  console.log(socket.client.id + ' is connected');


  socket.on('lost', function (data) {

    console.log("voici les données : " + data);
    socket.broadcast.emit('popup', { element: data, type: 'delete' });

  })

  socket.on('found', function (data) {

    console.log("voici les données : " + data);
    socket.broadcast.emit('popup', { element: data, type: 'found' });

  })


  /* Disconnect socket */
  socket.on('disconnect', function () {

    count--;
    io.emit('counter', { count: count });
    console.log("client is disconnected");

  });
});

server.listen(8080, () => console.log(`Listening on port 8080...`));