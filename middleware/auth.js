const jwt = require("jsonwebtoken");
const config = require("config");

module.exports = {

  /* FONCTIONNEL */
  /* authentification vérifie qu'un token jwt existe dans le header (mais là on va vérifier qu'iL est dans le LocalStorage)
   (sauf que ici le module utilisé express-session simule le storage et conserve les données côté serveur (mais ça fonctionne pareil)
   Si le token existe, le middleware vérifie ce dernier , le décode et vérifie l'existance du compte
   si tout est bon, il passe le token décodé dans le next() via res.locals (La norme) soit à un autre middleware soit à la route associée) */
  authentification: async function (req, res, next) {
    console.log(req)
    console.log(res)
    let possibleTokenHead = ['Bearer']
    //var token = req.session.accessToken /* sera remplacé pus tard par un request.headers['Authorization'] */
    var token = req.headers.authorization /* voilà c'est remplacé :D */

    if (!token) return res.status(403).send({ message: "Acces refusé, pas de token" });

    var tokenInformations = token.split(' ')

    if (!possibleTokenHead.includes(tokenInformations[0]))
      return res.status(403).send("Token incorrect");

    try {
      /*
            let result = await axios.get('http://localhost:3000/apiexterne/get_public_key');
      
            if (result.status == '200') {
              const decoded = jwt.verify(tokenInformations[1], config.get("cle"), function (err, decoded) {
                if (err) {
                  return res.status(403).send({ message: 'Token invalide' });
                } else {
                  res.locals.account = decoded;
                  next();
                }
              });
            }
      
            else {
              res.status(400).send({
                message: "API d'authentification : " + result.message
              })
            }
      */
      if (tokenInformations[1] == "tokenOK")
        next();
      else {
        return res.status(400).send({
          message: "UNE ERREUR DE MERDE"
        })
      }
    } catch (err) {
      res.status(400).send({ message: err.data });
    }
  }
}