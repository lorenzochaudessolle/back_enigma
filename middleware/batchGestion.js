const bunker = require("../models/bunker")
const cesar = require("../utils/algo")
module.exports = {

  /* FONCTIONNEL */
  /* le middleware accountVerify permet de vérifier si L'Account du token est Le même que L'Account utiLisé dans la requête */
  /* Pour savoir au final, si l'utilisateur connecté manipule bien son compte */
  batchCreation: async function (req, res, next) {

    if(bunker.secretMessages.length < 1)
      return res.status(404).send({ message: "Aucun batch n'est disponible" })


    let randNb = Math.floor(Math.random() * bunker.secretMessages.length);
    let randomMessage = bunker.secretMessages[randNb];

    req.batch = { secretMessage: randomMessage, minKey: 1, maxKey: 25 };
    next();
  },


  batchVerification: async function (req, res, next) {


    if (req.body.secretMessage == "" || req.body.decalage == "" || req.body.messageDechiffre == "") {
      return res.status(404).send({ message: "Les données ne sont pas correctement entrées" })
    }


    let test = cesar(req.body.secretMessage, req.body.decalage)

    if (test != req.body.messageDechiffre.toUpperCase())
      return res.status(400).send({ message: "Le message n'est pas bien déchiffré" })


    next();
  },



}