const config = require("config");
const express = require("express");
var cors = require('cors');
const app = express();
const CesarRoute = require("./routes/cesar.route");


//use config module to get the privatekey, if no private key set, end the application
if (!config.get("cle")) {
  console.error("FATAL ERROR: la clé secrète n'est pas définie.");
  process.exit(1);
}

/* Les headers suivants autorisent uniquement l'origine localhost
ainsi que quelques headers de l'expéditeur client (notamment Authorization qui permet au client d'envoyer le Token)
Access-Control-Allow-Credentials autorise l'envoie de cookies ou d'autres données utilisateurs (ici Authorization)
*//*
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  next();
});*/

app.use(cors({
  allowedHeaders: ['authorization', 'content-type'],
  'origin': 'http://localhost',
}));

app.use(express.json());
//app.use(session({ secret: 'session_secret' }));

//use users route for api/users
app.use("/api/cesar", CesarRoute);

const port = 3000;

app.listen(port, () => console.log(`Listening on port ${port}...`));