
const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth").authentification;

const { batchCreation, batchVerification } = require("../middleware/batchGestion");
const bunker = require("../models/bunker")
const algos = require("../models/algos").algos

/* FONCTIONNEL */
router.get("/codeOnDemand/:langageKey", auth, async (req, res) => {
  //application/javascript
  let result = algos.find(x => x.language == req.params.langageKey)
  if (result == null) {
    return res.status(404).send({message: "Langage pas disponible"})
  }

  res.status(200).send(result.func)

});


/* localhost:3000/api/cesar/validationSlug */

/* FONCTIONNEL */
router.get("/validationSlug", auth, async (req, res) => {

  res.status(200).send(bunker.validationSlug)

});

/* localhost:3000/api/cesar/batch */
/* FONCTIONNEL */
router.get("/batch", auth, batchCreation, async (req, res) => {
  res.status(200).send(req.batch)

});


/* FONCTIONNEL */
router.post("/lost", auth, async (req, res) => {

  if (bunker.secretMessages.includes(req.body.secretMessage)) {
    bunker.secretMessages.splice(bunker.secretMessages.indexOf(req.body.secretMessage), 1);

    res.status(200).send({
      message: "batch retiré de la liste avec succès"
    });
  }
  else {
    res.status(404).send({
      message: "batch plus disponible"
    });
  }
});


/* FONCTIONNEL */
router.post("/found", auth, batchVerification, async (req, res) => {

  if (bunker.secretMessages.includes(req.body.secretMessage)) {
    bunker.secretMessages.splice(bunker.secretMessages.indexOf(req.body.secretMessage), 1);
    res.status(200).send({
      message: "Message déchiffré avec succés"
    });
  }
  else {

    res.status(404).send({
      message: "Batch plus disponible"
    });
  }

});



module.exports = router;